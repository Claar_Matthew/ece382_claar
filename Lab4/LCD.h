#ifndef LCD_H_
#define LCD_H_


void initLCD();


void bootLCD(const char* intro, const char* name);


void updateLCD(uint16_t angle, char* strAngle);


void LCDsendstr(const char* my_str, int8_t len);


void LCDsendcomm(const int16_t c);

#endif /* LCD_H_ */
