#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

// Include LCD header file
#include <Controller.h>

void initController(){
    P2DIR &= ~BIT6;                              // Set P2.6 as an input
    P2SEL &= ~BIT6;                              // Use as GPIO
    P2SEL2 &= ~BIT6;                             // Use as GPIO
    P2REN |= BIT6;                               // Enable pull up/down resistor
    P2OUT |= BIT6;

    P2IFG &= ~BIT6;                              // Clear any interrupt flag on P2.6
    P2IE  |= BIT6;                               // Enable P2.6 interrupt

    P1DIR |= BIT0|BIT6|BIT5;                     // Set LEDs as outputs
    P1OUT &= ~(BIT0|BIT6|BIT5);                  // Clear LED pins

    TA1CCR0 = 16000;                             // create a 16ms roll-over period
    TA1CTL &= ~TAIFG;                            // clear flag before enabling interrupt
    TA1CTL |= ID_3|TASSEL_2|MC_1;                // Use 1:8 prescalar off SMCLK and enable interrupts

    __enable_interrupt();                        // enable interrupts
}
