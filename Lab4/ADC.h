#ifndef ADC_H_
#define ADC_H_


void initADC();


uint16_t angleConversion(uint16_t analog);


uint16_t ADC(uint16_t angle);


void itoa(int16_t value, char* result, uint16_t base);

#endif /* ADC_H_ */
