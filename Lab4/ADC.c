#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

#include <ADC.h>


void initADC(){
    ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
    ADC10CTL1 = INCH_4;                         // input A4
    ADC10AE0 |= BIT4;                           // P1.4 ADC Analog enable
    ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;         // Select SMCLK

    __enable_interrupt();
}


uint16_t ADC(uint16_t angle){
    uint16_t dutyCycle = 0;    // Convert to counts
    if (angle == 0){
        dutyCycle = 250;
    }
    else if(angle == 45){
        dutyCycle = 355;
    }
    else if(angle == 90){
        dutyCycle = 605;
    }
    else if(angle == 135){
        dutyCycle = 910;
    }
    else{
        dutyCycle = 1210;      // Servo at 180 degrees
    }
    return dutyCycle;
}


uint16_t angleConversion(uint16_t analog){
    uint16_t angle = 0;     // assume the angle is 0

    if (analog >= 160 && analog < 240){
        angle = 45;
    }
    else if (analog >= 400 && analog < 480){
        angle = 90;
    }
    else if (analog >= 640 && analog < 720){
        angle = 135;
    }
    else if (analog >= 880 && analog < 1500){
        angle = 180;
    }
    else{
        angle = 0;
    }
    return angle;
}


void itoa(int16_t value, char* result, uint16_t base){  // Authored by Maj Walckho, converts int into str
      // check that the base if valid
      if (base < 2 || base > 36) { *result = '\0';}

      char* ptr = result, *ptr1 = result, tmp_char;
      int16_t tmp_value;

      do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
      } while ( value );

      *ptr-- = '\0';
      while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
      }
}
