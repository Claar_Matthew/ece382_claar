#include <msp430g2553.h>
#include <stdbool.h>
#include <stdint.h>

#include <PWM.h>


void initPWM(){
    P1SEL &= ~BIT5;                 // Setup P1.5 as output
    P1SEL2 &= ~BIT5;
    P1DIR |= BIT5;                  // Set servo as output
    P1OUT &= ~BIT5;                 // Clear servo pin

    TA0CCR0 = 20000;                // Creates a 20ms roll-over period
    TA0CCR1 = 500;                  // initialize the duty cycle
    TA0CTL |= ID_0|TASSEL_2|MC_1;   // Use 1:8 prescalar off SMCLK and enable interrupts

    TA0CCTL0 |= CCIE;               // enable CC interrupts
    TA0CCTL1 |= OUTMOD_7|CCIE;      // set TACCTL1 to Set / Reset mode//enable CC interrupts
    TA0CCTL1 &= ~CCIFG;             //clear capture compare interrupt flag

    __enable_interrupt();
}


void turnServo(uint16_t dutyCycle){
    TA0CCR1 = dutyCycle;
}
