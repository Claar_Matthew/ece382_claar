/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive St 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : main.c
   AUTHOR(S)     : C2C Matthew Claar
   DATE          : 11/30/2018
   COURSE        : ECE 382, M3 Maj Walchko

   LAB #4        : LCD & control servo with potentiometer

   DOCUMENTATION : C2C Garrett Gwozdz helped correct a timer selection issue when I explained a build error to him.

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/
#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

#include <LCD.h>
#include <PWM.h>
#include <ADC.h>
#include <Controller.h>

uint16_t angle = 0;
uint16_t dutyCycle = 0;
const char intro[] = "ECE 382, Fall 2018";
const char name[] = "C2C Matthew Claar";


int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    DCOCTL = 0;                 // Init clock @ 1 MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    initLCD();
    bootupLCD(intro, name);
    initController();
    initADC();
    initPWM();


    while (1){      // POT Functionality
        ADC10CTL0 |= ENC + ADC10SC;                 // Sampling and conversion start
        __bis_SR_register(CPUOFF + GIE);            // Force exit
        angle = angleConversion(ADC10MEM);          // Convert angle
        dutyCycle = ADC(angle);                     // Convert to duty cycle from angle

        char strAngle[] = "\0";                     // Create a printable angle
        itoa(angle, strAngle, 10);

        updateLCD(angle, strAngle);                 // Re-write LCD screen
        __delay_cycles(2000000);                    // Delay 2 seconds in between updates

        turnServo(dutyCycle);                       // Turn servo

    } // End infinite while loop
}

#pragma vector=ADC10_VECTOR            // ADC10 interrupt service routine
__interrupt void ADC10_ISR(void)
{
  __bic_SR_register_on_exit(CPUOFF);   // Clear CPUOFF bit from 0(SR)
}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void captureCompareInt (void) {
    P1OUT |= BIT5;                     //Turn on Servo
    TA0CCTL1 &= ~CCIFG;                //clear CCI flag
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void captureCompareInt2 (void) {
    P1OUT &= ~BIT5;                    //Turn off Servo
    TA0CCTL1 &= ~CCIFG;                //clear CCI flag
}

