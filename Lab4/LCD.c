
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

#include <LCD.h>


void initLCD(){
    P1SEL |= BIT2;
    P1SEL2 |= BIT2;

    UCA0CTL1 = UCSWRST;                     // Hold the USCI

    UCA0CTL1 |= UCSSEL_2;                   // select BRCLK = SMCLK
    UCA0CTL0 = 0;                           // 8N1
    UCA0BR0 = 104;                          // p 424 1MHz@9600
    UCA0BR1 = 0;                            // p 424 1MHz@9600
    UCA0MCTL = UCBRS0;                      // UCBRSx=1

    UCA0CTL1 = ~UCSWRST;                    // Take USCI out of reset
}


void LCDsendstr(const char* my_str, int8_t len){
    int i = 0;
    for (i = 0; i < len; i++){
        while (!(IFG2 & UCA0TXIFG));        // USCI_A0 TX buffer ready?
        UCA0TXBUF = my_str[i];              // TX
    }
}


void LCDsendcomm(const int c){
    while (!(IFG2 & UCA0TXIFG));            // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;                          // TX
}


void bootLCD(const char* intro, const char* name){
    // Clear the LCD screen
    LCDsendstr(0xFE);                     // CTR
    LCDsendcomm(0x01);                     // CMD

    int i = 0;
    for (i = 0; i < len; i++){
        __delay_cycles(1000000);
    }

    LCDsendstr(intro, 18);                // Display "ECE 328, Fall 2018" on LCD
    __delay_cycles(1000000);

    LCDsendcomm(0xFE);                     // Move cursor to next line to print name

    __delay_cycles(1000000);

    LCDsendstr(name, 16);                 // Display name on screen
    __delay_cycles(2000000);                // Delay 2 seconds

    LCDsendcomm(0xFE);                     // Clear LCD
    LCDsendcomm(0x01);

    LCDsendstr("Servo: 0 deg", 12);       // Replace with servo angle
}


void updateLCD(uint16_t angle, char* strAngle){
    LCDsendcomm(0xFE);                       // Clear screen
    LCDsendcomm(0x01);

    LCDsendstr("Servo: ", 7);               // Show servo angle

    if (angle > 99){                          // Sends appropriate value for servo angle size
        LCDsendstr(strAngle, 3);
    }
    else if (angle > 10){
        LCDsendstr(strAngle, 2);
    }
    else{
        LCDsendstr(strAngle, 1);
    }
}
