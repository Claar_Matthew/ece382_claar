;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
DATA:       .byte 0x22, 0x33, 0x11, 0x44
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
main:
	mov #DATA, r4
	cmp #DATA, 0x11
	jeq b11
	cmp 2(#DATA), 0x22
	jeq b22
	cmp 3(#DATA), 0x33
	jeq b33
	cmp 4(#DATA), 0x44
	jeq b44

b11:
	push byte 0xC0
	jump main

b22:
	push 0xDFEC
	jump main

b33:
	push byte 0x15
	ret

b44:
	push 1
	pop r9
	pop.b r8
	pop.b r7
	pop r6
	jump forever

forever: jump forever
;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
