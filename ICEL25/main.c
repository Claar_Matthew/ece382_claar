#include <msp430.h> 


/**
 * main.c
 */
char interruptFlag = 0;     // global variable?  Bad or good?
void main(void) {
    WDTCTL = WDTPW|WDTHOLD; // stop the watchdog timer
    P1DIR |= BIT0|BIT6;     // set LEDs to output
    P1DIR &= ~BIT3;         // set button to input
    P1REN |= BIT3;          // enable internal pull-up/pull-down network
    P1OUT |= BIT3;          // configure as pull-up
    P1IES |= BIT3;          // configure interrupt to sense falling edges
    P1IFG &= ~BIT3;         // clear P1.3 interrupt flag
    P1IE |= BIT3;           // enable the interrupt for P1.3
    __enable_interrupt();
    // main program loop
    while (1) {
        P1OUT ^= BIT6;
        __delay_cycles(1000000);
    }
}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1_ISR(void) {
    P1IFG &= ~BIT3;       // clear P1.3 interrupt flag
    P1OUT ^= BIT0;   // toggle LEDs
    interruptFlag = 1;    // you could use this tell the main() something
}
