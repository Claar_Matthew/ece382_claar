#include <msp430.h> 


/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	int i2c_transfer(uint8_t addr, uint8_t *buf, size_t nbytes){
	  UCB0I2CSA = addr;  // set slave address
	  UCB0CTL1 |= UCTR | UCTXSTT;  // set start condition

	  while ((UCB0CTL1 & UCTXSTT) && ((IFG2 & UCB0TXIFG) == 0));

	  err = _check_ack(dev);  // check for ACK � defined elsewhere

	  while ((err == 0) && (nbytes > 0)) {
	        UCB0TXBUF = *buf;
	        while ((IFG2 & UCB0TXIFG) == 0) {
	            err = _check_ack(dev);
	            if (err < 0) break;
	        }
	        buf++;  // grab next byte
	        nbytes--;
	    }
	  return err;  // 0: good, -1: error occurred
	}

	
	return 0;
}
