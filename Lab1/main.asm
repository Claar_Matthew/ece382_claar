;-------------------------------------------------------------------------------
; Lab 1: Assembly Calculator
; C2C Matthew Claar
; ECE 382, M7
; 09 September 2018
;
; Documentation: I received no additional help on this assignment.
;
; This is a basic assembly calculator created for the MSP430
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
;DATA:		.byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55
;DATA:		.byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55
;DATA:		.byte	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
DATA:		.byte	0xff, 0x33, 0xff, 0x44, 0xEE, 0x22, 0xff, 0x33, 0xff, 0x55


ADD_OP:		.equ	0x11
SUB_OP:		.equ	0x22
MUL_OP:		.equ	0x33
CLR_OP:		.equ	0x44
END_OP:		.equ	0x55
MAX_NUM:	.equ	0xFF
MIN_NUM:	.equ	0x00

			.data
myResults:	.space	20
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
main:
	mov.w #DATA, r5		; Copies base ROM address into r5
	mov.w #myResults, r7; Copies base RAM address into r7
	mov.b @r5+, r12		; Copies first operand into r12 (result)

continue:
	mov.b @r5+, r6		; Copies operator into r6

	cmp #END_OP, r6		; compares operator to end and jumps if equal
	jz CPU_TRAP

	mov.b @r5+, r13		; Copies second operand into r13

	cmp #CLR_OP, r6		; compares operator to clr and jumps if op is zero
	jz CLR

	cmp #ADD_OP, r6		; compares operator to add and jumps if op is zero
	jz addNum

	cmp #SUB_OP, r6		; compares operator to sub and jumps if op is zero
	jz subNum

	cmp #MUL_OP, r6		; compares operator to mul and jumps if op is zero
	jz MUL

	jmp CLR				; Performs CLR operation if operator is unrecognized

CLR:
	mov.b r13, r12		; Copies next operand into r12 (operand 1)
	mov.b #0, 0(r7)		; Stores hex 0 in ROM (clears current value)
	inc r7				; Increments ROM for next input
	jmp continue		; Continues main loop

addNum:
	add.w r13, r12		; Add two operands, move to results subroutine
	jmp Results			; Stores value in ROM

subNum:
	sub r13, r12		; Subtract two results, move to results subroutine
	jmp Results			; Stores value in ROM

MUL:
	;cmp #0xFF, r12		; If either operand exceeds max, set result to max
	;jz ExceedMax
	;cmp #0xFF, r13
	;jz ExceedMax
	cmp #0, r12			; If either operand is zero, jump and set result to zero
	jz mult_zero
	cmp #0, r13
	jz mult_zero
	mov.b r12, r14		; Loop counter (multiply through addition)
	clr r12				; Preps r12 for addition loop
	add.w r13, r12		; Adds first instance and jumps to add loop
	jmp addLoop

addLoop:				; Loop to "multiply" through an add operation
	dec r14
	jz Results			; If counter is zero, moves to store result in ROM
	add.w r13, r12		; Otherwise, continue adding
	jmp addLoop

mult_zero:				; Copies hex 0 into operand 1 (results) and moves to store in ROM
	mov.w #0x00, r12
	jmp Record

Results:				; Checks if result is > than max or <= min
	cmp #MAX_NUM+1, r12	; If greater than max, moves to store max val in ROM
	jge ExceedMax
	cmp #MIN_NUM, r12	; If less than or equal to min, moves to store min val in ROM
	jl BelowMin

Record:					; Stores current operand (result) in r7 (ROM)
	mov.b r12, 0(r7)
	inc r7				; Increments r7 to be ready to accept the next value
	jmp continue		; Jumps back to main loop to continue sequence

ExceedMax:
	mov.b #MAX_NUM, r12	; Copies max num into r12 to be stored in ROM
	jmp Record

BelowMin:
	mov.b #MIN_NUM, r12	; Copies min val into r12 to be stored in ROM
	jmp Record

CPU_TRAP:	jmp CPU_TRAP

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
