;--+----------------------------------------------------------------------------
;--|
;--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
;--|
;--| United States Air Force Academy     __  _______ ___    _________
;--| Dept of Electrical &               / / / / ___//   |  / ____/   |
;--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
;--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
;--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;--|
;--| ---------------------------------------------------------------------------
;--|
;--| FILENAME      : main.asm
;--| AUTHOR(S)     : C2C Matthew Claar
;--| CREATED       : 09/26/2018
;--| DESCRIPTION   : Stoplight in Assembly
;--|
;--| DOCUMENTATION : None
;--|
;--+----------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
				; Measure button bouncing time
				; Set up active-low button for input
				bic.b #BIT3, &P1SEL   ; GPIO
				bic.b #BIT3, &P1SEL2  ; GPIO
				bic.b #BIT3, &P1DIR   ; pin set to input
				bis.b #BIT3, &P1REN   ; resistor enabled
				bis.b #BIT3, &P1OUT   ; pull-up

				; DCO calibration
				bis.b #BIT4, &P1DIR ; show SMCLK on P1.4
				bis.b #BIT4, &P1SEL

				bic.b #BIT3|BIT4|BIT5, &P2SEL ; serial, srclk, rclk
				bic.b #BIT3|BIT4|BIT5, &P2SEL2
				bic.b #BIT3|BIT4|BIT5, &P2DIR
				bic.b #BIT3|BIT4|BIT5, &P2REN
				bic.b #BIT3|BIT4|BIT5, &P2OUT

clear			clr	  r4					; clears register and output pins
				bic.b #BIT3|BIT4|BIT5, &P2OUT

buttonStat		bit.b #BIT3, &P1IN
				jz	  buttonStat1
				jmp   buttonStat

buttonStat1		bit.b #BIT3, &P1IN		; polls for first button press (press & release)
				jz    buttonStat1
				bis.b #BIT3, &P2OUT
				call  #buttonPress
				jmp   buttonStat

buttonPress		push  r5				; First button press
				cmp   #0x000, r4		; Stores value in reg (button that is on)
				jnz   LED1
				call  #set1				; Calls subroutines to send on/off bits
				call  #set0				; for each LED
				call  #set0
				call  #set0
				jmp   end

LED1			cmp   #0x0001, r4
				jnz   LED2				; Same as above, LED2
				call  #set0
				call  #set1
				call  #set0
				call  #set0
				jmp   end

LED2			cmp   #0x0002, r4
				jnz   LED3				; Same as above, LED3
				call  #set0
				call  #set0
				call  #set1
				call  #set0

LED3			cmp   #0x0003, r4
				jnz   end				; Same as above, LED4
				call  #set0
				call  #set0
				call  #set0
				call  #set1

end				bis.b #BIT5, &P2OUT		; toggles rclk
				bic.b #BIT5, &P2OUT
				inc   r4
				cmp   #0x0004, r8
				jnz   delay1			; moves to copy delay time, moves to software delay (debouncing)
				clr   r4

delay1			mov.w #0xBBFE, r5	; delay time

softwareDelay	dec   r5
				jnz   softwareDelay
				pop   r5
				ret

set1			bis.b #BIT3, &P2OUT			; sets an LED high
				bis.b #BIT4, &P2OUT
				bic.b #BIT4, &P2OUT
				ret

set0			bic.b #BIT3, &P2OUT			; sets an LED low
				bis.b #BIT4, &P2OUT
				bic.b #BIT4, &P2OUT
				ret

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect     .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
