//--+----------------------------------------------------------------------------
//--|
//--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
//--|
//--| United States Air Force Academy     __  _______ ___    _________
//--| Dept of Electrical &               / / / / ___//   |  / ____/   |
//--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
//--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
//--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
//--|
//--| ---------------------------------------------------------------------------
//--|
//--| FILENAME      : main.c
//--| AUTHOR(S)     : C2C Matthew Claar
//--| CREATED       : 11/05/2018
//--| DESCRIPTION   : RE IR Remote
//--|
//--| DOCUMENTATION : None
//--|
//--+----------------------------------------------------------------------------
#include <msp430.h>
#include <stdint.h>
#include <stdbool.h>

// delay code written in assembly
// make sure to drag/drop the assembly code too
extern void Delay160ms(void);
extern void Delay40ms(void);

// macros to make life easier
#define        IR_PIN            (P2IN & BIT6)
#define        HIGH_2_LOW        P2IES |= BIT6
#define        LOW_2_HIGH        P2IES &= ~BIT6

// Globals are bad, but these are constants and can't be changed accidentally.
// Set them up for your remote.
const uint32_t Button1 = 0x85;
const uint32_t Button2 = 0x89;
const uint32_t Button3 = 0x8E;
const uint32_t Button4 = 0x82;
const uint32_t Button5 = 0x87;
uint16_t irData = 0;
uint16_t index = 0;


// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void main(void){

    WDTCTL=WDTPW+WDTHOLD;  // stop WDT

    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;
    TA0CTL |= TACLR;                                        // Fill in the next six lines of code.
    TA0CCR0 |= 16000;                                       // 16ms roll-over period
    TA0CTL |= TASSEL_2 | MC_1 | ID_3 | TAIFG;               // Use 1:8 pre-scalar off SMCLK and enable interrupts

    P2DIR &= ~BIT6;     // Set up P2.6 as GPIO not XIN
    P2REN |= BIT6;
    P2SEL &= ~BIT6;
    P2SEL2 &= ~BIT6;
    P2OUT  |= BIT6;

    P2IFG &= ~BIT6;     // Clear interrupt flag on P2.3
    P2IE  |= BIT6;      // Enable P2.3 interrupt

    HIGH_2_LOW;         // check the header out.  P2IES changed.

    P1DIR |= BIT0|BIT6|BIT1|BIT2;                                           // Set LEDs as outputs
    P1OUT &= (~BIT0); P1OUT &= (~BIT6); P1OUT &= (~BIT1); P1OUT &= (~BIT2); // And turn the LEDs off



    P1OUT ^= BIT6;                    // toggle the Green led for on for 1 second
    _delay_cycles(8000000);
    P1OUT ^= BIT6;

    __enable_interrupt();             // and enable interrupts

    while(1){
       if(index == 10){
           __disable_interrupt();
           P1OUT |= BIT0;             // Flash on-board red LED when a button is pressed
           switch(irData)
           {
           case Button1:              // Turn on LED1 if Button1
               P1OUT |= BIT1;
               break;
           case Button2:              // Turn off LED1 if Button2
               P1OUT &= ~BIT1;
               break;
           case Button3:              // Flash LED2 3 times if Button3
           {
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               break;
           }
           case Button4:              // Flash LED2 5 times if Button2
           {
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               P1OUT ^= BIT2;
               __delay_cycles(8000000);
               break;
           }
           case Button5:              // Toggle LED1 and LED2 if Button 5
               P1OUT ^= (BIT1); P1OUT ^= (BIT2);
               break;
           default:
               break;
               }
           index = 0;
           Delay160ms();
           Delay160ms();
           P1OUT &= ~BIT0;            // Turn off on-board Red LED
           __enable_interrupt();
           }
    }
}



// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR
__interrupt void pinChange (void) {
    P1OUT |= BIT0;                  // turn on red Led
    uint8_t    pin;
    uint16_t   pulseDuration;          // The timer is 16-bits
    const uint16_t tLogic1 = 1000;

    if (IR_PIN)  // is pin high or low?
        pin=1;
    else
        pin=0;

    switch (pin) {
        case 0: // !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
            pulseDuration = TA0R;

           if(index > 1)
           {
               if(pulseDuration>tLogic1)
                   irData |= 1<<(9-(index%10));
               else
                   irData &= ~1<<(9-(index%10));
           }
           index++;
           TA0CTL &= ~TAIE;
           LOW_2_HIGH;             // Set up pin interrupt on positive edge
           break;

        case 1: // !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
           TA0R = 0x0000;          // time measurements are based at time 0
           TA0CTL |= MC_1 | TAIE;
           HIGH_2_LOW;             // Set up pin interrupt on falling edge
           break;
    } // end switch

   P2IFG &= ~BIT6;              // Clears the interupt flag
   P1OUT &= ~BIT0;              // Turns off the red Led

} // end pinChange ISR


#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
    TA0CTL &= ~MC_1;              // Turn off Timer A and Enable Interrupt
    TA0CTL &= ~TAIE;
    TA0CTL &= ~TAIFG;  // clear timer a interrupt
}
